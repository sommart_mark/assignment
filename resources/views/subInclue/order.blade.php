<fieldset id="detail" style="display: none">
<div class="container">
<div class="panel-group" id="accordion">
<div class="panel">
<div id="collapse1" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ไอศกรีมเค้ก
<span class="hidden-xs" style="position: absolute;right: 0px;">
<a href="https://www.swensens1112.com/th/product/cake" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/cake" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Festive-Cake" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/423/2749.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> สุขเต็มปอนด์ </h1>
<p class="product-detail">
ไอศกรีมรสวานิลลาและช็อกโกแลต แบล็ค ฟอเรส
</p>
<p class="product-detail2">
พร้อมซอสมิกซ์เบอร์รี่<br><span style="color: red;">พิเศษสำหรับบัตรสมาชิก<br></span><span style="color: red;">ในราคา 699 บาท</span>
</p>
<h3 class="product-price text-center">3 ปอนด์ | 879 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Festive-Cake" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Choco-Oreo" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/230/9621.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อกโก โอรีโอ </h1>
<p class="product-detail">
ไอศกรีมรสช็อคโกแลต
</p>
<p class="product-detail2">
และคุกกี้ แอนด์ ครีม
<br><span style="color:red">พิเศษสำหรับบัตรสมาชิก</span>
<br><span style="color:red">ซื้อในราคา 399 บาท</span>
</p>
<h3 class="product-price text-center">1.5 ปอนด์ | 529 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Choco-Oreo" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
 </a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Very-strawberry" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/231/4050.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> เวรี่ สตรอเบอร์รี่ </h1>
<p class="product-detail">
ไอศกรีมรสเวรี่ สตรอเบอร์รี่
</p>
<p class="product-detail2">
และคุกกี้ แอนด์ ครีม
<br><span style="color:red">พิเศษสำหรับบัตรสมาชิก</span>
<br><span style="color:red">ซื้อในราคา 399 บาท</span>
</p>
<h3 class="product-price text-center">1.5 ปอนด์ | 529 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Very-strawberry" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Triple-Flavors" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/25/3147.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ทริปเปิ้ล เฟล์เวอร์ </h1>
<p class="product-detail">
ไอศกรีมรสเวรี สตรอเบอร์รี่
</p>
<p class="product-detail2">
วานิลลา และช็อคโกแลต<br><span style="color: red;">พิเศษสำหรับบัตรสมาชิก</span> <br><span style="color: red;">ซื้อในราคา 699 บาท</span><br>
</p>
<h3 class="product-price text-center">3 ปอนด์ | 879 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Triple-Flavors" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Strawberry-Delight" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/27/6126.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> สตรอเบอร์รี่ ดีไลท์ </h1>
<p class="product-detail">
ไอศกรีมรสสตรอเบอร์รี่ ชีสเค้ก
</p>
<p class="product-detail2">
เวรี่ สตรอเบอร์รี่ และคุกกี้แอนด์ครีม
<br><span style="color: red;">พิเศษสำหรับบัตรสมาชิก</span> <br><span style="color: red;">ซื้อในราคา 699 บาท</span><br>
</p>
<h3 class="product-price text-center">3 ปอนด์ | 879 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Strawberry-Delight" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Ultimate-Chocolate" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/24/6589.jpg');"></div>
</div>
</a>
 <div class="card-info">
<h1 class="product-name height_name"> อัลติเมทช็อกโกแลต </h1>
<p class="product-detail">
ไอศกรีมรสสติคกี้ ชูวี่ ช็อคโกแลต
</p>
<p class="product-detail2">
วานิลลา และสติคกี้ ชูวี่ ช็อคโกแลต
<br><span style="color: red;">พิเศษสำหรับบัตรสมาชิก</span> <br><span style="color: red;">ซื้อในราคา 699 บาท</span><br>
</p>
<h3 class="product-price text-center">3 ปอนด์ | 879 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Ultimate-Chocolate" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Chocolate-Cookies-&#039;n-Cream" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/30/7028.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อกโกแลต คุกกี้ แอนด์ ครีม </h1>
<p class="product-detail">
ไอศกรีมรสคุกกี้แอนด์ครีม
</p>
<p class="product-detail2">
และช็อคโกแลต
<br><span style="color: red;">พิเศษสำหรับบัตรสมาชิก</span> <br><span style="color: red;">ซื้อในราคา 699 บาท</span><br>
</p>
<h3 class="product-price text-center">3 ปอนด์ | 879 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Chocolate-Cookies-&#039;n-Cream" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
 </div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/cake/Miracle-four" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/29/1173.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มิราเคิล 4 </h1>
<p class="product-detail">
ไอศกรีมรสเวรี่ สตรอเบอร์รี่, วานิลลา
</p>
<p class="product-detail2">
ช็อคโกแลต และคุกกี้แอนด์ครีม
<br>
<br>
<br>
</p>
<h3 class="product-price text-center">4 ปอนด์ | 995 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/cake/Miracle-four" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/cake" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse2" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ไอศกรีมควอท (450g.)
<span class="hidden-xs" style="position: absolute;right: 15px;">
<a href="https://www.swensens1112.com/th/product/icecream" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/icecream" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
 </span>
</h2>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/Chocolate Black Forest-Quart" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/419/6212.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อกโกแลต แบล็ค ฟอเรส </h1>
<p class="product-detail">
ไอศกรีมช็อกโกแลตเข้มข้มสูตรพิเศษ
</p>
<p class="product-detail2">
พร้อมชิ้นเชอร์รี่และบลูเบอร์รี่
<p></p>
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/Chocolate Black Forest-Quart" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/raspberry-lemon-pie-quart" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/409/5211.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ราสเบอร์รี่ เลมอน พาย </h1>
<p class="product-detail">
ไอศกรีมหวานอมเปรี้ยวแบบเลมอน
</p>
<p class="product-detail2">
พร้อมเนื้อราสเบอร์รี่สด และพายกรุบกรอบ
 </p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/raspberry-lemon-pie-quart" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/fruity-sherbet-quart" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/411/7909.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ฟรุ้ตตี้ เชอร์เบท </h1>
<p class="product-detail">
ไอศกรีมเปรี้ยวลงตัวด้วยมะนาวและเลมอน
</p>
<p class="product-detail2">
หวานสดชื่นด้วยสตรอเบอร์รี่และส้มสด
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/fruity-sherbet-quart" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/Mango-Sorbet" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/23/8501.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วง ซอร์เบท์ </h1>
<p class="product-detail">
 ไอศกรีมซอร์เบท์ รสมะม่วงน้ำดอกไม้
</p>
<p class="product-detail2">
ผสมเนื้อมะม่วงสุก
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/Mango-Sorbet" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/Apple-Cranberry-Sorbet" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/16/4820.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> แอปเปิ้ล แครนเบอร์รี่ ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมซอร์เบท์ ไม่มีไขมัน
</p>
<p class="product-detail2">
ผสมชิ้นแอปเปิ้ล และแครนเบอรี่แช่อิ่ม
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/Apple-Cranberry-Sorbet" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/Dragon-Fruit-Mango" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/304/3082.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วงแก้วมังกร ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมรสหวานอมเปรี้ยว 
</p>
<p class="product-detail2">
ลงตัวระหว่างมะม่วงและแก้วมังกร<br>
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/Dragon-Fruit-Mango" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/green-mango-sorbet" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/276/6121.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วงเขียว ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมเปรี้ยว อร่อยลงตัว 
</p>
<p class="product-detail2">
จากมะม่วงเขียวคัดพิเศษ
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/green-mango-sorbet" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
 <div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream/Lime-Sherbet" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/43/7303.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะนาว เชอร์เบท </h1>
<p class="product-detail">
ไอศกรีมผสมมะนาว
</p>
<p class="product-detail2">
<br>
</p>
<h3 class="product-price text-center">1 ควอท | 329 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream/Lime-Sherbet" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/icecream" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse_pint" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ไอศกรีมมินิ ควอท (250g.)
<span class="hidden-xs" style="position: absolute;right: 15px;">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
 <div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Chocolate Black Forest-Mini Quart" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/420/8441.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อกโกแลต แบล็ค ฟอเรส </h1>
<p class="product-detail">
ไอศกรีมช็อกโกแลตเข้มข้มสูตรพิเศษ
</p>
<p class="product-detail2">
พร้อมชิ้นเชอร์รี่และบลูเบอร์รี่<br>
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Chocolate Black Forest-Mini Quart" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/raspberry-lemon-pie-mini-quart" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/397/3202.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ราสเบอร์รี่ เลมอน พาย </h1>
<p class="product-detail">
ไอศกรีมหวานอมเปรี้ยวแบบเลมอน
</p>
<p class="product-detail2">
พร้อมเนื้อราสเบอร์รี่สด และพายกรุบกรอบ
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
 </div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/raspberry-lemon-pie-mini-quart" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/fruity-sherbet-miniquart" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/402/5948.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ฟรุ้ตตี้ เชอร์เบท </h1>
<p class="product-detail">
ไอศกรีมเปรี้ยวลงตัวด้วยมะนาวและเลมอน
</p>
<p class="product-detail2">
หวานสดชื่นด้วยสตรอเบอร์รี่และส้มสด
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/fruity-sherbet-miniquart" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Mango-Sorbet-Pint" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/377/7725.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วง ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมซอร์เบท์ รสมะม่วงน้ำดอกไม้
</p>
 <p class="product-detail2">
ผสมเนื้อมะม่วงสุก
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Mango-Sorbet-Pint" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Apple-Cranberry-Sorbet-Pint" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/385/9735.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> แอปเปิ้ล แครนเบอร์รี่ ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมซอร์เบท์ ไม่มีไขมัน
</p>
<p class="product-detail2">
ผสมชิ้นแอปเปิ้ล และแครนเบอรี่แช่อิ่ม
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Apple-Cranberry-Sorbet-Pint" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Dragon-Fruit-Mango-Pint" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/365/6748.jpg');"></div>
</div>
</a>
 <div class="card-info">
<h1 class="product-name height_name"> มะม่วงแก้วมังกร ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมรสหวานอมเปรี้ยว 
</p>
<p class="product-detail2">
ลงตัวระหว่างมะม่วงและแก้วมังกร<br>
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Dragon-Fruit-Mango-Pint" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Green-Mango-Sorbet-Pint" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/376/7816.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วงเขียว ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมเปรี้ยว อร่อยลงตัว 
</p>
<p class="product-detail2">
จากมะม่วงเขียวคัดพิเศษ
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Green-Mango-Sorbet-Pint" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Lime-Sherbet-Pint" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/362/3331.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะนาว เชอร์เบท </h1>
<p class="product-detail">
ไอศกรีมผสมมะนาว <br>
</p>
<p class="product-detail2">
<br>
</p>
<h3 class="product-price text-center">1 มินิควอท | 199 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/ice-cream-pint/Lime-Sherbet-Pint" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/ice-cream-pint" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse-icecream-bar" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ไอศกรีมบาร์
<span class="hidden-xs" style="position: absolute;right: 15px;">
<a href="https://www.swensens1112.com/th/product/icecream" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/icecream" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
 <a href="https://www.swensens1112.com/th/product/icecream-bar/Mango-Multipack" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/303/1869.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์มะม่วง มัลติแพ็ค 4 แท่ง </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 249 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/Mango-Multipack" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-vanilla-multipack" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/300/4235.jpeg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์รสวานิลลา มัลติแพ็ค 4 แท่ง </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 219 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-vanilla-multipack" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
 <div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-mocha-multipack" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/302/1330.jpeg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์รสมอคค่า มัลติแพ็ค 4 แท่ง </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 249 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-mocha-multipack" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-cookie-and-cream-multipack" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/301/9388.jpeg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์รสคุกกี้แอนด์ครีม มัลติแพ็ค 4 แท่ง </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 249 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-cookie-and-cream-multipack" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-mango" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/299/7481.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์มะม่วง </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 69 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-mango" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/icecream-bar-vanilla" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/248/1852.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์ รสวานิลลา </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 แท่ง | 59 บาท</h3>
</div>
</div>
 <a href="https://www.swensens1112.com/th/product/icecream-bar/icecream-bar-vanilla" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-mocha" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/247/5596.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์ รสมอคค่า </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 แท่ง | 69 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-mocha" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-cookie-cream" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/249/1159.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ไอศกรีมบาร์ คุกกี้ แอนด์ ครีม </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 แท่ง | 69 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/icecream-bar/ice-cream-bar-cookie-cream" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/icecream-bar" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse3" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ท้อปปิ้ง
<span class="hidden-xs" style="position: absolute;right: 15px;">
<a href="https://www.swensens1112.com/th/product/Topping" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/Topping" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Cherry-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/424/4386.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> เชอร์รี่ โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Cherry-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Almond-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/425/6066.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> อัลมอนด์ โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Almond-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Chocolate-Chip-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/426/8744.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อกโกแลตชิพ โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
 </p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Chocolate-Chip-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Rainbow-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/427/3711.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> เรนโบว์ โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Rainbow-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Mixed-Nut-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/428/7332.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มิกซ์นัท โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">

</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Mixed-Nut-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Oreo-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/429/1836.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> โอรีโอ โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Oreo-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Strawberry-Jam-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/431/9433.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> แยมสตรอว์เบอร์รี โอเวอร์โหลด </h1>
<p class="product-detail">
 </p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Strawberry-Jam-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Topping/Caramel-Overload-Topping" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/432/9021.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> คาราเมล โอเวอร์โหลด </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 49 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Topping/Caramel-Overload-Topping" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/Topping" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse4" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ชุดปาร์ตี้เซ็ต
</h2>
<h2 class="heading color_red text-center visible-xs">
</h2>
<div class="flex-wrapper product-wrapper">
</div>
<h2 class="heading color_red text-center">
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse5" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">บัตรกำนัลเงินสด
</h2>
<h2 class="heading color_red text-center visible-xs">
</h2>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-100-Baht" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/3/5936.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสด 100 บาท </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 100 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-100-Baht" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-200-Baht" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/73/1799.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสด 200 บาท </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 200 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-200-Baht" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-Ice-Cream-Cake-855" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/176/2069.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสดไอศกรีมเค้ก 855 บาท </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 855 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-Ice-Cream-Cake-855" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-Ice-Cream-Cake-995" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/177/3920.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสดไอศกรีมเค้ก 995 บาท </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 995 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/Vouchers/Gift-Voucher-Ice-Cream-Cake-995" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/E-Vouchers/e-Voucher" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/13/2362.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสดแบบอิเล็กทรอนิกส์ </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 100 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/E-Vouchers/e-Voucher" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/E-Vouchers/E-Gift-Voucher-Ice-Cream-Cake-855" class="order-link">
 <div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/178/5092.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสดไอศกรีมเค้กแบบอิเล็กทรอนิกส์ 855 บาท </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 855 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/E-Vouchers/E-Gift-Voucher-Ice-Cream-Cake-855" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/E-Vouchers/E-Gift-Voucher-Ice-Cream-Cake-995" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/179/7217.png');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บัตรกำนัลเงินสดไอศกรีมเค้กแบบอิเล็กทรอนิกส์ 995 บาท </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ใบ | 995 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/E-Vouchers/E-Gift-Voucher-Ice-Cream-Cake-995" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
 <h2 class="heading color_red text-center">
</h2>
</div>
</div>
</div>
<div class="panel">
<div id="collapse7" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ไอศกรีมสกู๊ป
<span class="hidden-xs" style="position: absolute;right: 15px;">
<a href="https://www.swensens1112.com/th/product/scoop" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/scoop" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<p class="heading color_red text-center">
</p>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Chocolate Black Forest-scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/421/9508.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อกโกแลต แบล็ค ฟอเรส </h1>
<p class="product-detail">
ไอศกรีมช็อกโกแลตเข้มข้มสูตรพิเศษ
</p>
<p class="product-detail2">
พร้อมชิ้นเชอร์รี่และบลูเบอร์รี่<br>
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Chocolate Black Forest-scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
 <div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Raspberrylemonpie-scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/398/1619.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ราสเบอร์รี่ เลมอน พาย </h1>
<p class="product-detail">
ไอศกรีมหวานอมเปรี้ยวแบบเลมอน
</p>
<p class="product-detail2">
พร้อมเนื้อราสเบอร์รี่สด และพายกรุบกรอบ
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Raspberrylemonpie-scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/fruity-sherbet-scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/401/4020.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ฟรุ้ตตี้ เชอร์เบท </h1>
<p class="product-detail">
ไอศกรีมเปรี้ยวลงตัวด้วยมะนาวและเลมอน
</p>
<p class="product-detail2">
หวานสดชื่นด้วยสตรอเบอร์รี่และส้มสด
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
 </div>
<a href="https://www.swensens1112.com/th/product/scoop/fruity-sherbet-scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Mango-Sorbet-Scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/206/7849.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วง ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมซอร์เบท์ รสมะม่วงน้ำดอกไม้
</p>
<p class="product-detail2">
ผสมเนื้อมะม่วงสุก
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Mango-Sorbet-Scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Apple cranberries-Sorbet-Scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/205/4663.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> แอปเปิ้ล แครนเบอร์รี่ ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมซอร์เบท์ ไม่มีไขมัน
</p>
<p class="product-detail2">
 ผสมชิ้นแอปเปิ้ล และแครนเบอรี่แช่อิ่ม
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Apple cranberries-Sorbet-Scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Dragon-Fruit-Mango-Scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/309/1672.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วงแก้วมังกร ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมรสหวานอมเปรี้ยว 
</p>
<p class="product-detail2">
ลงตัวระหว่างมะม่วงและแก้วมังกร<br>
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Dragon-Fruit-Mango-Scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Green-Mango-Sorbet-Scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/278/4628.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะม่วงเขียว ซอร์เบท์ </h1>
<p class="product-detail">
ไอศกรีมเปรี้ยว อร่อยลงตัว 
</p>
<p class="product-detail2">
จากมะม่วงเขียวคัดพิเศษ
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Green-Mango-Sorbet-Scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/scoop/Lime-Sherbet-Scoop" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/204/8915.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> มะนาว เชอร์เบท </h1>
<p class="product-detail">
ไอศกรีมผสมมะนาว <br>
</p>
<p class="product-detail2">
<br>
</p>
<h3 class="product-price text-center">1 สกู๊ป | 65 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/scoop/Lime-Sherbet-Scoop" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/scoop" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
 </div>
<div class="panel">
<div id="collapse6" class="panel-collapse collapse">
<div class="panel-body" style="position: relative;">
<h2 class="heading color_red text-center">ซันเด เซต
<span class="hidden-xs" style="position: absolute;right: 15px;">
<a href="https://www.swensens1112.com/th/product/sundae-set" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<h2 class="heading color_red text-center visible-xs">
<span class="">
<a href="https://www.swensens1112.com/th/product/sundae-set" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
<p class="heading color_red text-center">
</p>
<div class="flex-wrapper product-wrapper">
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Festive Duo Sundae Set" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/422/9020.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> สุขดับเบิ้ล ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Festive Duo Sundae Set" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Chocolate-Lava-Cake" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/296/9072.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อคโกแลต ลาวาเค้ก ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 179 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Chocolate-Lava-Cake" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Choco-Brownie" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/295/4828.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> ช็อคโก บราวนี่ ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Choco-Brownie" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Blueberry-Cheese-Cake-Waffle" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/271/3450.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> บลูเบอร์รี่ ชีสเค้ก วาฟเฟิล ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Blueberry-Cheese-Cake-Waffle" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Very-Stawberry-Waffle" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/272/5311.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> เวรี่ สตรอเบอร์รี่ วาฟเฟิล ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Very-Stawberry-Waffle" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Sticky-Chewy-Choc" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/273/2328.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> สติกกี้ ชูวี่ช็อค ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Sticky-Chewy-Choc" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/์์nutty -caramel-waffle" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/274/3992.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> นัตตี้ คาราเมล วาฟเฟิล </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/์์nutty -caramel-waffle" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
 </div>
<div class="flex-item">
<div class="product-card">
<div class="card-wrapper">
<a href="https://www.swensens1112.com/th/product/sundae-set/Oreo-Brownie" class="order-link">
<div class="card-image">
<div class="image" style="background-image: url('https://cms.swensens1112.com/image/product/275/4899.jpg');"></div>
</div>
</a>
<div class="card-info">
<h1 class="product-name height_name"> โอรีโอ บราวนี่ส์ ซันเด เซต </h1>
<p class="product-detail">
</p>
<p class="product-detail2">
</p>
<h3 class="product-price text-center">1 ชุด | 159 บาท</h3>
</div>
</div>
<a href="https://www.swensens1112.com/th/product/sundae-set/Oreo-Brownie" class="button_global_write text-center bg_red color_white buy_now">
สั่งซื้อ
</a>
</div>
</div>
</div>
<h2 class="heading color_red text-center">
<span class="">
<a href="https://www.swensens1112.com/th/product/sundae-set" class="color_red hover_red" style="font-size:0.8em;">ดูทั้งหมด >></a>
</span>
</h2>
</div>
</div>
</div>
</div>
</div>
</fieldset>
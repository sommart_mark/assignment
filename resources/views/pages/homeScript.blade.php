<script src="https://www.swensens1112.com/js/swiper.min.js"></script>
<script>
    $('.image').click(function() {
        $('#detail').show();
    });
    $('.slider').slick({
        slidesToScroll: 1,
        slidesToShow: 2,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 3000,
    });
    var swiper = new Swiper('#banner', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 3000,
        },
    });
    var swiper = new Swiper('#menu', {
        // Default parameters
        slidesPerView: 5.5,
        freeMode: true,
        spaceBetween: 40,
        // Responsive breakpoints
        breakpoints: {
            // when window width is <= 320px
            270: {
                slidesPerView: 1.8,
            },
            300: {
                slidesPerView: 2,
            },
            360: {
                slidesPerView: 2.1,
            },
            400: {
                slidesPerView: 2.5,
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 2.7,
            },
            // when window width is <= 640px
            640: {
                slidesPerView: 3.3,
            },
            678: {
                slidesPerView: 4,
            },
            720: {
                slidesPerView: 4.5,
            },
            1200: {
                slidesPerView: 4.5,
            }
        }
    });
</script>
<script src="https://www.swensens1112.com/js/shuffle.js"></script>
<script>
    $(document).ready(function() {

        (function(global) {

            if (typeof(global) === "undefined") {
                throw new Error("window is undefined");
            }

            var _hash = "!";
            var noBackPlease = function() {
                global.location.href += "#";

                // making sure we have the fruit available for juice (^__^)
                global.setTimeout(function() {
                    global.location.href += "!";
                }, 50);
            };

            global.onhashchange = function() {
                if (global.location.hash !== _hash) {
                    global.location.hash = _hash;
                }
            };

            global.onload = function() {
                noBackPlease();

                // disables backspace on page except on input fields and textarea..
                document.body.onkeydown = function(e) {
                    var elm = e.target.nodeName.toLowerCase();
                    if (e.which === 8 && (elm !== 'input' && elm !== 'textarea')) {
                        e.preventDefault();
                    }
                    // stopping event bubbling up the DOM tree..
                    e.stopPropagation();
                };
            }

        })(window);
    });
</script>


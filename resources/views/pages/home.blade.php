@extends('layouts.default')
@section('content')
    <div class="container hidden-xs">
        <div class="row position" style="width: 100%;">
            <div class="search_tab display" id="search">
                <form action="https://www.swensens1112.com/th/product/search" method="get">
                    <input type="text" class="form-control search" id="text_search" name="search" placeholder="ค้นหาจาก ชื่อสินค้า, ประเภทสินค้า, อื่นๆ">
                        <img src="https://www.swensens1112.com/icon/swensens_icon001.png" class="icon_search " class="icon_footer">
                        <div class="" style="position: relative;">
                        <ul id="tab_search">
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="content-wrap">

        <div class="page page-home">
        <!-- <script type="text/javascript" src="{{ asset('js/snow-effect.js')  }}"></script>
        <script>goAdorSnow('', 1000, '', false, false, false, 0.05, 0.05, 0.01);</script> -->
            <div class="session section-banner-slider">
                <div class="banner-slider">
                    <div class="swiper-container swiper-container-horizontal" id="banner">
                    <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next " data-swiper-slide-index="4">
                        <a href="https://www.swensens1112.com/th/product/icecream" style="background-image: url(https://cms.swensens1112.com/image/banner/98/3331.jpg)">
                        </a>
                    </div>
                    <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next " data-swiper-slide-index="4">
                        <a href="https://www.swensens1112.com/th/product/cake" style="background-image: url(https://cms.swensens1112.com/image/banner/92/8697.jpg)">
                        </a>
                    </div>
                    <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next " data-swiper-slide-index="4">
                        <a href="https://www.swensens1112.com/th/product/cake" style="background-image: url(https://cms.swensens1112.com/image/banner/33/7638.jpg)">

                        </a>
                    </div>
                </div>

                <div class="swiper-pagination"></div>
                </div>
                </div>
            </div>

            <div class="section section-product-list">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-menu">
                                <h2 class="heading color_red">
                                สั่งออนไลน์
                                </h2>
                                <div class="swiper-wrapper hidden-xs hidden-sm" style="justify-content: space-between;">
                                <div class="col-md-1 text-center ">
                                    <img src="https://www.swensens1112.com/icon_home/swensens_pic-01.png" id="image1" class="image icon_product" aria-expanded="false" data-group="Cake-Ice-Cream" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                </div>
                                <div class="col-md-1 text-center">
                                    <img src="https://www.swensens1112.com/icon_home/swensens_quart_th.png" id="image2" class="image icon_product" aria-expanded="false" data-group="Quattro-Ice-Cream" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                </div>
                                <div class="col-md-1 text-center">
                                    <img src="https://www.swensens1112.com/icon_home/swensens_mini_quart_th.png?v=3" id="image_pint" class="image icon_product" aria-expanded="false" data-group="ice-cream-pint" data-toggle="collapse" data-parent="#accordion" href="#collapse_pint">
                                </div>
                                <div class="col-md-1 text-center">
                                    <img src="https://www.swensens1112.com/icon_home/sundae-set2-th.png?v=3" id="take-away-th" class="image icon_product" aria-expanded="false" data-group="sundae-set" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                                </div>
                                <div class="col-md-1 text-center">
                                    <img src="https://www.swensens1112.com/icon_home/swensens_pic-21.png" id="image21" class="image icon_product" aria-expanded="false" data-group="Scoop-Ice-Cream" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                                </div>
                                <div class="col-md-1 text-center">
                                    <img src="https://www.swensens1112.com/icon_home/swensens_pic-03.png" id="image3" class="image icon_product" aria-expanded="false" data-group="Topping" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                </div>
                                <div class="col-md-1 text-center">
                                    <img src="https://www.swensens1112.com/icon_home/icecream-bar-th.png?v=1" id="icecream-bar-th" class="image icon_product" aria-expanded="false" data-group="icecream-bar" data-toggle="collapse" data-parent="#accordion" href="#collapse-icecream-bar">
                                </div>
                                <div class="col-md-2 text-center ">
                                    <img src="https://www.swensens1112.com/icon_home/swensens_pic-05.png" id="image5" class="image icon_product" aria-expanded="false" data-group="Vouchers" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                </div>
                            </div>

                    <section class="our-icecream-slider slider visible-xs visible-sm">
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/swensens_pic-01.png" id="image1" class="img-responsive image" aria-expanded="false" data-group="Cake-Ice-Cream" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/swensens_quart_th.png" id="image2" class="img-responsive image" aria-expanded="false" data-group="Quattro-Ice-Cream" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/swensens_mini_quart_th.png?v=3" id="image_pint" class="img-responsive image" aria-expanded="false" data-group="ice-cream-pint" data-toggle="collapse" data-parent="#accordion" href="#collapse_pint">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/sundae-set2-th.png?v=3" id="take-away-th-mobile" class="img-responsive image" aria-expanded="false" data-group="sundae-set" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/icecream-bar-th.png?v=1" id="icecream-bar-th-mobile" class="img-responsive image" aria-expanded="false" data-group="icecream-bar" data-toggle="collapse" data-parent="#accordion" href="#collapse-icecream-bar">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/swensens_pic-21.png" id="image21" class="img-responsive image" aria-expanded="false" data-group="Scoop" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/swensens_pic-03.png" id="image3" class="img-responsive image" aria-expanded="false" data-group="Topping" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                            </a>
                        </div>
                        <div>
                            <a href="#">
                                <img src="https://www.swensens1112.com/icon_home/swensens_pic-05.png" id="image5" class="img-responsive image" aria-expanded="false" data-group="Vouchers" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                            </a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @include('subInclue.order')

    <section class="how-to-sign-up">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-sm-5 text-center">
                            <img src="https://www.swensens1112.com/assets/images/AW_Swensen FC Card 2020 Ol_Front.png?v=3" class="how-to-sign-up-card img-responsive" alt="">
                        </div>
                        <div class="col-sm-7">
                            <div class="good text-center">
                                <table class="text-left how-to-sign-up-table">
                                <tr>
                                    <td class="table-img">
                                        <img src="https://www.swensens1112.com/icon/swensens_e-36.png" class="icon_home">
                                    </td>
                                    <td>รับฟรี! ไอศกรีม 1 สกู๊ป (เมื่อลงทะเบียนบัตรบนเว็บไซต์)</td>
                                </tr>
                                <tr>
                                    <td class="table-img">
                                        <img src="https://www.swensens1112.com/icon/swensens_e-35.png" class="icon_home">
                                    </td>
                                    <td>ซื้อ 1 แถม 1 สกู๊ป ทุกวันอังคาร</td>
                                </tr>
                                <tr>
                                    <td class="table-img">
                                        <img src="https://www.swensens1112.com/icon/swensens_e-37.png" class="icon_home">
                                    </td>
                                    <td>ส่วนลด 10% จากราคาปกติ</td>
                                </tr>
                                <tr>

                                </tr>
                                </table>
                                <button class="btn-how-to-sign-up text-center bg_white color_red" onclick="window.location.href='https://www.swensens1112.com/th/swensens-card'">วิธีการสมัครและรับสิทธิ์</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
    </div>

    @include('pages/homeScript')
    
    </div>
@stop
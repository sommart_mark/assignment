<div id="sidenav" class="sidenav-short-height hidden-lg hidden-md" style="display: none;">
    <div class="sidenav-inner">
        <div class="sidenav-scoll">
            <div class="sidenav-header">
                <div class="nav-slide-logo text-center">
                    <a href=" / "><img src="{{ asset('/image/logo.png') }}" alt=""></a>
                </div>
            <div class="sidenav-close">
                <button type="button" class="button-close-sidenav"><span class="color_red close_button">&times;</span>
                </button>
            </div>
        </div>
            <div class="sidenav-body">
                <ul class="sidenav-menu">
                    <ul class="submenu">
                        <li class="visible-xs">
                            <a href="https://www.swensens1112.com/th/mobile/search" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_search.png" class="icon_footer">ค้นหา</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/cake" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_cake.png" class="icon_footer">ไอศกรีมเค้ก</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/icecream" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_quart.png" class="icon_footer">ไอศกรีมควอท (450g.)</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/ice-cream-pint" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_quart.png" class="icon_footer">ไอศกรีมมินิ ควอท (250g.)</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/sundae-set" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_takeaway.png" class="icon_footer">ซันเด เซต</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/scoop" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_scoop.png" class="icon_footer">ไอศกรีมสกู๊ป</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/Topping" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_topping.png" class="icon_footer">ท้อปปิ้ง</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/icecream-bar" class=""><img src="https://www.swensens1112.com/icon/swensens_icecream_bar.png" class="icon_footer">ไอศกรีมบาร์</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/product/Vouchers" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_gift_voucher.png" class="icon_footer">บัตรกำนัลเงินสด</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/swensens-card" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_member_card.png" class="icon_footer">สมาชิกสเวนเซ่นส์การ์ด</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/brandsite" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_story.png" class="icon_footer">เรื่องราวของสเวนเซ่นส์</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/about-us" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_aboutus.png" class="icon_footer">เกี่ยวกับเรา</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/brandsite/shop/search" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_finder.png" class="icon_footer">ค้นหาร้านใกล้คุณ</a>
                        </li>
                        <li>
                            <a href="https://www.swensens1112.com/th/member/login" id="login_" class=""><img src="https://www.swensens1112.com/icon/swensens_menu_login.png" class="icon_footer">เข้าสู่ระบบ</a>
                        </li>
                    </ul>
                </ul>
            </div>
            <div class="sidenav-footer">
                <div class="lang-switcher">
                <ul>
                    <li>
                        <a href=" " class=" active ">TH</a>
                    </li>
                    <li style="border-left: 1px solid #828282;">
                        <a href="/en/" class="">EN</a>
                    </li>
                </ul>
                </div>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
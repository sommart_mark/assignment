<div id="footer">
<div class="footer-top">
<div class="container">
<div class="footer-top-wrapper">
<div class="credit-card">
<ul>
<li>
<img src="https://www.swensens1112.com/assets/images/visa.svg" alt="">
</li>
<li>
<img src="https://www.swensens1112.com/assets/images/mastercard.svg" alt="">
</li>
<li>
<img src="https://www.swensens1112.com/assets/images/jcb.svg" alt="">
</li>
</ul>
</div>
</div>
</div>
<div class="footer-bottom">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-12 section-contact-us">
<div class="contact-us">
<div class="social-list hidden-xs hidden-sm">
<ul>
<li class="list-heading">
<p class="heading contact">ติดต่อเรา</p>
</li>
<li class="tel">
<a href="tel:1112">
<span>
<img src="https://www.swensens1112.com/brandsite/images/call.png" class="icon_footer"><span class="heading"> </span>
</span>
</a>
</li>
<li class="tel" style="display: none">
<a href="https://www.swensens1112.com/th/contactus">
<span>
<img src="https://www.swensens1112.com/images/icon_mail.png" class="icon_footer"><span class="heading"> </span>
</span>
</a>
</li>
<li class="list-heading">
<p class="heading">ติดตามเรา</p>
</li>
<li>
<a href="https://www.facebook.com/weloveswensens/" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/facebook.png" class="icon_footer">
</a>
</li>
<li>
<a href="https://www.instagram.com/we_love_swensens/" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/instragram.png" class="icon_footer">
</a>
</li>
<li>
<a href="https://www.youtube.com/channel/UCZg-uMHcpiqr3z1qiJCurIQ/videos" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/youtube.png" class="icon_footer">
</a>
</li>
<li>
<a href="line://ti/p/@qic0396o" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/footernew.png" class="icon_footer">
</a>
</li>
</ul>
</div>
<div class="social-list visible-xs visible-sm col-sm-12">
<p class="heading text-center">ติดต่อเรา</p>
<div class="col-md-12 text-center">
<ul style="display: inline-block;">
<li>
<a href="tel:1112">
<img src="https://www.swensens1112.com/brandsite/images/call.png" class="icon_footer">
</a>
</li>
<li style="display: none">
<a href="https://www.swensens1112.com/th/contactus">
<img src="https://www.swensens1112.com/images/icon_mail.png" class="icon_footer">
</a>
</li>
</ul>
</div>
<div class="social-list visible-xs visible-sm col-sm-12">
<div class="">
<p class="heading text-center">ติดตามเรา</p>
</div>
</div>
<div class="col-md-12 text-center">
<ul style="display: inline-block;">
<li>
<a href="https://www.facebook.com/weloveswensens/" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/facebook.png" class="icon_footer">
</a>
</li>
<li>
<a href="https://www.instagram.com/we_love_swensens/" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/instragram.png" class="icon_footer">
</a>
</li>
<li>
<a href="https://www.youtube.com/channel/UCZg-uMHcpiqr3z1qiJCurIQ/videos" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/youtube.png" class="icon_footer">
</a>
</li>
<li>
<a href="line://ti/p/@qic0396o" target="_blank">
<img src="https://www.swensens1112.com/brandsite/icon/footernew.png" class="icon_footer">
</a>
</li>
</ul>
</div>
</div>
</div>
<div class="delivery-available hidden-xs hidden-sm">
<img src="https://www.swensens1112.com/icon/swensens_menu2_cs6-33.png" class="clockicon_footer">จัดส่งสินค้าเวลา 10:00 น. - 21:00 น.&nbsp;รับออเดอร์สุดท้ายเวลา 20:00 น.
</div>
<div class="delivery-available visible-xs visible-sm col-xs-12 col-sm-12">
<div class="timeopen">
<img src="https://www.swensens1112.com/icon/swensens_menu2_cs6-33.png" class="icon_footer"> จัดส่งสินค้าเวลา 10:00 น. - 21:00 น.
<span class="timeshop" style="display: block;">รับออเดอร์สุดท้ายเวลา 20:00 น.</span>
</div>
</div>
</div>
<div class="col-md-6 col-sm-12 hidden-xs hidden-sm">
<div class="footer-menu">
<ul>
<li>
<a href="https://www.swensens1112.com/th/about-us" class="">เกี่ยวกับเรา</a>
</li>
<li> 
<a href="https://www.swensens1112.com/th/faq" class="">คำถามที่พบบ่อย</a>
</li>
<li>
<a href="https://www.swensens1112.com/th/terms-and-conditions" class="">ข้อตกลงและเงื่อนไข</a>
</li>
<li>
<a href="https://www.swensens1112.com/th/privacy-policy" class="">นโยบายความเป็นส่วนตัว</a>
</li>
</ul>
</div>
<div class="copyright">
COPYRIGHT © 2018 SWENSEN'S THAILAND. ALL RIGHTS RESERVED.
</div>
</div>
</div>
<div class="col-sm-12 visible-xs visible-sm">
<div class="footer-menu footer_menu">
<div class="col-xs-6 text-center">

<a href="https://www.swensens1112.com/th/faq" class="">คำถามที่พบบ่อย</a>
</div>
<div class="col-xs-6 text-center">

<a href="https://www.swensens1112.com/th/terms-and-conditions" class="">ข้อตกลงและเงื่อนไข</a>
</div>
<div class="col-xs-12 text-center">

<a href="https://www.swensens1112.com/th/privacy-policy" class="">นโยบายความเป็นส่วนตัว </a>
</div>
</div>
<div class="copyright copyright_mobile">
<div>
COPYRIGHT © 2018 SWENSEN'S THAILAND. ALL RIGHTS RESERVED.
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<form id="logout-form" action="https://www.swensens1112.com/th/logout" method="POST" style="display: none;">
<input type="hidden" name="_token" value="Bbd2M5apHeEhHRjcY1EmVnylkaToamp6FsQHxtDY">
</form>
@include('pages/homeFooterScript')
<noscript> 
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCPHK5N" height="0" width="0" style="display:none;visibility:hidden"></iframe> 
</noscript>
<div id="header">
    <div class="header-inner is-fixed has-nav border_mobile" style="">
        <div class="container-fluid header-padding">
            <div class="container">
                <div class="wrapper">
                    <div class="header-l">
                        <button type="button" class="menu-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="bar"></span>
                            <span class="bar"></span>
                            <span class="bar"></span>
                        </button>
                    <div class="lang-switcher hidden-xs">
                        <div class="toggle">
                            <a href="#en">EN</a>
                        </div>
                        <ul class="list hidden-sm">
                            <li>
                                <a href="https://www.swensens1112.com/th/brandsite" class="red-button" style="color:white; padding: 5px 15px 5px 15px; margin-right: 10px;" target="_blank">เรื่องราวของสเวนเซ่นส์</a>
                            </li>

                            <li>
                                <a href=" " class=" active ">TH</a>
                            </li>
                            <li style="border-left: 1px solid #828282;">
                                <a href="/en/" class="">EN</a>
                            </li>
                        </ul>
                    </div>
                </div>
            <div class="logo">
                <a href=" / ">
                    <img src="{{ asset('/image/logo.png') }}" class="nav-img">
                </a>
            </div>
            <div class="header-r">
                <div class="account hidden-sm">
                    <a href="https://www.swensens1112.com/th/member/login" id="login_" class="login-menu top_margin"><img src="https://www.swensens1112.com/brandsite/icon/iconregister.png" class="icon_footer">เข้าสู่ระบบ</a>
                </div>
                <div class="bag hidden-xs" style="margin-right: 35px;">
                    <img src="https://www.swensens1112.com/brandsite/icon/searchicon1.png" id="icon_search" class="icon_footer" data-toggle="collapse" data-parent="#accordion" href="#search" style="cursor: pointer;">
                </div>
                <div class="bag visible-xs visible-sm bag-sm" style="margin-right: 32px; margin-top: 2px;">
                    <a href="https://www.swensens1112.com/th/member/login">
                <img src="https://www.swensens1112.com/icon/SWS_icon01.png" class="icon_footer" style="cursor: pointer;">
                    </a>
                </div>
                <div id="bag" class="bag dropdown">
                    <a href="https://www.swensens1112.com/th/cart" class="must_login">
                <img src="https://www.swensens1112.com/icon/iconbag1.png" class="icon_footer">
                </a>
            </div>
        </div>
    </div>
</div>
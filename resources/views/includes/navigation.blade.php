<div class="navigation">
    <ul class="nav" id="side-menu">
        <li class="">
            <a href="https://www.swensens1112.com/th/product/cake" class="" lang="th">ไอศกรีมเค้ก</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/icecream" class="" lang="th">ไอศกรีมควอท (450g.)</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/ice-cream-pint" class="" lang="th">ไอศกรีมมินิ ควอท (250g.)</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/sundae-set" class="" lang="th">ซันเด เซต</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/scoop" class="" lang="th">ไอศกรีมสกู๊ป</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/Topping" class="" lang="th">ท้อปปิ้ง</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/icecream-bar" class="" lang="th">ไอศกรีมบาร์</a>
        </li>
        <li class="">
            <a href="https://www.swensens1112.com/th/product/Vouchers" class="" lang="th">บัตรกำนัลเงินสด</a>
        </li>
        <li class="card " style="background-color: #b78009; padding-right: 7px; ">
            <a href="https://www.swensens1112.com/th/swensens-card" lang="th"><img src="https://www.swensens1112.com/icon/swensens_menu_member_card_40x40.png" class="icon_nav">สมาชิกสเวนเซ่นส์การ์ด</a>
        </li>
    </ul>
</div>

<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body id="body">
    @include('includes.header')
    @include('includes.navigation')
    @include('includes.sideNavigation')
    
    @yield('content')

    @include('includes.footer')
</body>
</html>